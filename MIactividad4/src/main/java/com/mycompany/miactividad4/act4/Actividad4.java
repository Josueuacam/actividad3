//How Much is True?
import java.util.stream.IntStream;

public class Challenge {
	public static int countTrue(boolean[] arr) {
		int coun = 0;
		for (boolean i : arr) if (i) coun += 1;
		return coun;
	}
}
//Exists a Number Higher?
public class ExistsANumberHigher {
	public static boolean existsHigher(int[] z, int t) {
		return java.util.Arrays.stream(z).reduce(0, (a,b)->a==1||b>=t?1:0)==1;
	}
}
//Capture the Rook
public class Challenge {
	public static boolean canCapture(String[] rooks) {
		return rooks[0].startsWith(rooks[1].substring(0,1)) || rooks[0].endsWith(rooks[1].substring(1));
	}
}
//Return the First Element of an Array
public class Challenge{
	public static int getFirstValue(int[] arr) {
		return arr[0];
	}
}
//Upvotes vs Downvotes
public class Solution {
	public static int getVoteCount(int upvotes,int downvotes) {
		return upvotes-downvotes;
	}
}